package claim

import (
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
)

func CreateToken(claim Claim, secret string) (string, error) {
	template := "%s%s%s"
	b, err := json.Marshal(claim)
	if err != nil {
		return "", errors.New("json marshall fail")
	}
	b64 := base64.URLEncoding.EncodeToString(b)
	signature := sign(b64, secret)
	return fmt.Sprintf(template, b64, Del, signature), nil
}
