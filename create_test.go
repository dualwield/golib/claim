package claim

import "testing"

func TestCreateToken(t *testing.T) {
	type args struct {
		claim  Claim
		secret string
	}
	tests := []struct {
		name    string
		args    args
		want    string
		wantErr bool
	}{
		{
			name: "basic",
			args: args{
				claim: Claim{
					Expiration: 123,
					Items:      nil,
					Key:        "TEST",
					Owner:      "BOB",
					Scope:      "XYZ",
					Value:      "UGH",
				},
				secret: "XYZ-123",
			},
			want:    "eyJleHAiOjEyMywia2V5IjoiVEVTVCIsIm93bmVyIjoiQk9CIiwic2NvcGUiOiJYWVoiLCJ2YWx1ZSI6IlVHSCJ9:ae5ae49b07b2db6d940fe024f37417a30ce199912a6bc01c88fc9c5ae2823388",
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := CreateToken(tt.args.claim, tt.args.secret)
			if (err != nil) != tt.wantErr {
				t.Errorf("CreateToken() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("CreateToken() got = %v, want %v", got, tt.want)
			}
		})
	}
}
