package claim

import (
	"encoding/base64"
	"encoding/json"
	"testing"
)

func TestValidateParsed(t *testing.T) {
	claim := Claim{
		Expiration: 0,
		Items:      nil,
		Key:        "abc",
		Owner:      "fred",
		Scope:      "xyz",
		Value:      "abc-xyz",
	}
	type args struct {
		pt     ParsedToken
		secret string
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{
			name: "simple validate",
			args: args{
				pt: ParsedToken{
					Claim:     claim,
					Payload:   payloadFromClaim(t, claim),
					Signature: "17d8e485f768d90e29f11d3b27c2c51fa744723226647ac40f7d29975400f05f",
				},
				secret: "xyz-123",
			},
			want: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := ValidateParsed(tt.args.pt, tt.args.secret); got != tt.want {
				t.Errorf("ValidateParsed() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_sign(t *testing.T) {
	claim := Claim{
		Expiration: 0,
		Items:      nil,
		Key:        "abc",
		Owner:      "fred",
		Scope:      "xyz",
		Value:      "abc-xyz",
	}
	type args struct {
		payload string
		secret  string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "sign",
			args: args{
				payload: payloadFromClaim(t, claim),
				secret:  "xyz-123",
			},
			want: "17d8e485f768d90e29f11d3b27c2c51fa744723226647ac40f7d29975400f05f",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := sign(tt.args.payload, tt.args.secret); got != tt.want {
				t.Errorf("sign() = %v, want %v", got, tt.want)
			}
		})
	}
}

func payloadFromClaim(t *testing.T, claim Claim) string {
	b, err := json.Marshal(claim)
	if err != nil {
		t.Fail()
	}
	return base64.URLEncoding.EncodeToString(b)
}
