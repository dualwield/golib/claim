package claim

import (
	"crypto/hmac"
	"crypto/sha256"
	"encoding/hex"
)

// ValidateParsed checks the actual signature of the ParsedToken's Claim's payload,
// against the expected signature
func ValidateParsed(pt ParsedToken, secret string) bool {
	return Validate(pt.Signature, pt.Payload, secret)
}

// Validate checks the actual signature of the payload against the expected signature
func Validate(actualSignature, payload, secret string) bool {
	expectedSignature := sign(payload, secret)
	return expectedSignature == actualSignature
}

func sign(payload, secret string) string {
	h := hmac.New(sha256.New, []byte(secret))
	h.Write([]byte(payload))
	return hex.EncodeToString(h.Sum(nil))
}
