package claim

type Claim struct {
	Expiration int64    `json:"exp,omitempty" toml:"exp"`
	Items      []string `json:"items,omitempty" toml:"items"`
	Key        string   `json:"key,omitempty" toml:"key"`
	Owner      string   `json:"owner,omitempty" toml:"owner"`
	Scope      string   `json:"scope,omitempty" toml:"scope"`
	Value      string   `json:"value,omitempty" toml:"value"`
}

type ParsedToken struct {
	Claim     Claim
	Payload   string
	Signature string
}
