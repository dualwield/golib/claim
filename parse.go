package claim

import (
	"encoding/base64"
	"encoding/json"
	"errors"
	"strings"
)

// Parse builds a ParsedToken instance from passed in tokenized claim
func Parse(tokenized string) (*ParsedToken, error) {
	if tokenized == "" {
		return nil, errors.New("empty")
	}
	if strings.Count(tokenized, Del) != 1 {
		return nil, errors.New("one delimiter pipe")
	}

	split := strings.Split(tokenized, Del)
	if split[0] == "" {
		return nil, errors.New("payload")
	}
	if split[1] == "" {
		return nil, errors.New("signature")
	}

	serializedClaim, err := base64.URLEncoding.DecodeString(split[0])
	if err != nil {
		return nil, errors.New("payload decoding")
	}

	var claim Claim
	if err := json.Unmarshal(serializedClaim, &claim); err != nil {
		return nil, errors.New("payload json")
	}
	return &ParsedToken{
		Claim:     claim,
		Payload:   split[0],
		Signature: split[1],
	}, nil
}
